# Zippopotam API Test Framework

Using Cucumber, Java, Serenity BDD and Rest Assured

The solution is based on the forked serenity-rest-starter project in Github.

## Description

This framework tests the endpoints of the Zippopotam demo API  that can be found at http://api.zippopotam.us/

Cucumber is used to be able to split functional description of the system under test in feature files from the technical implementation of the tests. Serenity
BDD is used to create reusable technical components. Serenity also delivers useful test reports that can be used as living documentation out of the box.
Serenity BDD has Rest Assured embedded as SerenityRest which is used to be able to quickly write components that talk to the API endpoints in a BDD (
given-when-then) way.

## Project structure

```Gherkin
src
  + test
    + java                                           Glue code and test runner
      + starter                                      Model of system under test
        + postcodes                                  Definition of order of teststeps
          LocationResponse.java                      Response message parser
          PostCodeAPI.java                           API interaction
          PostCodeStepDefinitions.java.              Definitions of steps
        CucumberTestSuite.java                       Test runner
    + resources
      logback-test.xml 
      + features
        + postcodes                              Feature files
          looking_up_post_codes.feature          Scenarios of functionality
        
          Serenity.conf                              Project configuration
```

## Installation

- Java 8, Maven and Git are required
- IntelliJ with Cucumber for Java plugin recommended
- Clone the project from https://gitlab.com/otaimre/serenity-rest

## Run

Use the following command to run all tests
```mvn clean verify```

### Run using Gitlab CI

1. Request access to this repository
2. Goto CI/CD -> Pipelines
3. Click Run Pipeline and then again Run pipeline (no extra params needed.)

### Run specific scenario

1. Add a tag to a scenario such as @runthis
1. Start the project using the command `mvn clean verify -Dcucumber.filter.tags=" @runthis"`

## Write new tests

To test more endpoints and functionality follow these steps

1. Add a feature file with a name and description of the feature or endpoint serenity-rest/src/test/resources/features
2. Write descriptive scenarios for each positive and negative scenario that needs to be automated
3. Check if Steps definitiuon already exists or create a new ones serenity-rest/src/test/java/starter/postcodes/PostCodeStepDefinitions.java
4. Reuse steps where possible
5. Add steps where needed

## Reports

After local excecution reports will be written to `target/site/serenity/index.html`
After excecution in GitLab (after every commit) reports will be saved as artefact in https://gitlab.com/otaimre/serenity-rest/-/pipelines

## Endpoint interactions

Interactions of this project with the API are written in the feature files.
