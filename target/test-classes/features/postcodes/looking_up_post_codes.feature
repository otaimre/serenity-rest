Feature: Looking up post codes

  Scenario Outline: [Positive] Looking up US locations by post code
    When I look up a post code <Post Code> for country code <Country Code>
    Then the resulting location should be <Place Name> in <Country>
    Examples:
      | Post Code | Country Code | Country       | Place Name    |
      | 10000     | US           | United States | New York City |
      | 90210     | US           | United States | Beverly Hills |




  Scenario Outline: [Negative] Looking up US location with wrong post code
    When I look up a post code <Post Code> for country code <Country Code>
    Then an error response is returned
    Examples:
      | Post Code | Country Code |
      | 100       | US           |
